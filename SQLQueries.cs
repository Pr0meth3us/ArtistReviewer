﻿namespace ArtistReviews
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    /// <summary>
    /// Collection of queries to database
    /// </summary>
    public class SQLQueries
    {
        /// <summary>
        /// Connection to server
        /// </summary>
        public SqlConnection Connection=new SqlConnection(ConfigurationManager.AppSettings["SQLConnectionString"]);

        /// <summary>
        /// Create reviewer.
        /// </summary>
        /// <param name="artistValue">Artist to review.</param>
        /// <param name="Reviewer">Reviewer name.</param>
        /// <param name="score">Score for said artist.</param>
        public void testCreateReview(string artistValue, string Reviewer, int score)
        {
            artistValue = artistValue.Replace(" ", "_");
            var cmdline = string.Format("INSERT INTO {0} (Reviewer,Score) VALUES (@val1,@val2)", artistValue);

                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = Connection;
                    createTable(artistValue);
                    command.CommandText = cmdline;
                    command.Parameters.AddWithValue("@val1", Reviewer);
                    command.Parameters.AddWithValue("@val2", score);
                    var count = "SELECT COUNT(*) FROM testArtist1 WHERE Reviewer = 'testReviewer1'";
                    using (SqlCommand exists = new SqlCommand(count, Connection))
                    {
                        var itexists = (int)exists.ExecuteScalar();
                        if (itexists > 0)
                        {
                            using (SqlCommand deleteold =
                                new SqlCommand(string.Format("DELETE FROM {0} WHERE Reviewer = '{1}' ", artistValue, Reviewer)))
                            {
                                deleteold.ExecuteNonQuery();
                            }

                        }
                    }

                    command.ExecuteNonQuery();
                    Console.WriteLine(string.Format("Succesfully created reviewer for {0} with score of {1}", Reviewer,
                        score));
                }
            
        }

        /// <summary>
        /// Creates a table for an artist.
        /// </summary>
        /// <param name="artistValue">The artist to create the table for.</param>
        public void createTable(string artistValue)
        {
                try
                {
                    var cmdline = string.Format("CREATE TABLE {0}(Reviewer varchar(50),Score int)", artistValue);
                    using (SqlCommand command = new SqlCommand(cmdline, Connection)) command.ExecuteNonQuery();
                }
                // TODO fix catch
                catch (SqlException sE) when (sE.Message == string.Format("There is already an object named '{0}' in the database.", artistValue))
                {
                }
            
        }

        /// <summary>
        /// Gets all empty tables i.e. all artists without reviews and pick one at random.
        /// </summary>
        public void getRandomEmptyBand()
        {

                List<string> tables = new List<string>();
                var tablelist = Connection.GetSchema("Tables");
                foreach (DataRow row in tablelist.Rows)
                {
                    string tablename = (string)row[2];
                    using (var dbCommand = new SqlCommand(string.Format("select Count(*) from {0}", tablename), Connection))
                    {
                        if ((int)dbCommand.ExecuteScalar() == 0)
                        {
                            tablename = tablename.Replace("_", " ");
                            tables.Add(tablename);
                        }
                    }
                }

                Random rnd = new Random();
                var randomArtist = rnd.Next(tables.Count);
                Console.WriteLine(tables[randomArtist]);
                Console.ReadLine();
            
        }

        /// <summary>
        /// Gets all artists.
        /// </summary>
        public void getAllBands()
        {

                List<string> tables = new List<string>();
                var tablelist = Connection.GetSchema("Tables");
                foreach (DataRow row in tablelist.Rows)
                {
                    string tablename = (string)row[2];
                    tablename = tablename.Replace("_", " ");
                    tables.Add(tablename);

                }

                tables.ForEach(i => Console.WriteLine("{0}\t", i));
                Console.ReadLine();
            
        }

        /// <summary>
        /// Gets all reviews for a specific band.
        /// </summary>
        /// <param name="artistValue">Artist to review.</param>
        /// <param name="averageRequest">If you want the average review</param>
        public void getAllReviewsOfBand(string artistValue, bool averageRequest = false)
        {
            artistValue = artistValue.Replace(" ", "_");
                var sqlQuery = string.Format("SELECT * FROM {0} ", artistValue);
                using (var allReviews = new SqlCommand(sqlQuery, Connection))
                {
                    SqlDataAdapter da = new SqlDataAdapter(allReviews);
                    List<int> listOfScores = new List<int>();
                    DataTable dt = new DataTable();
                    {
                        da.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {
                            Console.WriteLine(dr["Reviewer"] + " score for artistValue " + artistValue +
                                              " is :\t" + dr["Score"]);
                            Console.WriteLine("\n");
                            if (averageRequest)
                            {
                                listOfScores.Add(Convert.ToInt32(dr["Score"]));
                            }

                        }

                        if (averageRequest)
                        {
                            var averageScore = listOfScores.Sum() / listOfScores.Count;
                            Console.WriteLine(string.Format("The average score for artist {0}, was : {1}", artistValue, averageScore));


                        }
                        Console.ReadLine();
                    }

                }

            
        }

        /// <summary>
        /// deletes a review from a specific reviewer for a specific band.
        /// </summary>
        /// <param name="artistValue">The Artist.</param>
        /// <param name="Reviewer">The reviewer.</param>
        public void deleteReviewer(string artistValue, string Reviewer)
        {
            artistValue = artistValue.Replace(" ", "_");
            var value = ConfigurationManager.AppSettings["SQLConnectionString"];
                using (SqlCommand deleteold =
                    new SqlCommand(string.Format("DELETE FROM {0} WHERE Reviewer = '{1}' ", artistValue, Reviewer),
                        Connection))
                {
                    deleteold.ExecuteNonQuery();
                }
        }

        // TODO IMPLIMENT SOMEHOW
        public void createSQLDatabase()
        {
            Console.WriteLine(@"Input server name (for example SOLIDSO-MARSHTO\LOCALSERVER): ");
            var serverName = Console.ReadLine();
            Console.WriteLine("Input User name: ");
            var userName = Console.ReadLine();
            Console.WriteLine("Input Password");
            var Password = Console.ReadLine();
            using (SqlConnection conn = new SqlConnection(@"Server=" + serverName + ";User ID=" + userName + ";Password=" + Password + ";TrustServerCertificate=True;Connection Timeout=30;ConnectRetryCount=3;ConnectRetryInterval=1;"))
            {
                try
                {
                    conn.Open();
                    Console.WriteLine("Input new database name:");
                    var readDatabaseName = Console.ReadLine();
                    var query = new SqlCommand("CREATE DATABASE " + readDatabaseName, conn);
                    query.ExecuteNonQuery();
                    Console.WriteLine("Database {0} successfully created", readDatabaseName);
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("There was an error creating database, error was : " + e.Message);
                    Console.ResetColor();
                }
            }
        }

    }
}