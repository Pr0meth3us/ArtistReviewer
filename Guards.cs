﻿namespace ArtistReviews
{
    using System;

    /// <summary>
    /// Guards.
    /// </summary>
    public class Guards
    {

        /// <summary>
        /// Checks in object is null and/or empty
        /// </summary>
        /// <param name="objectToCheck">The object to check.</param>
        /// <param name="objectName">The object name.</param>
        public void Guard(object objectToCheck, string objectName)
        {
            if (objectToCheck == null)
            {
                throw new ArgumentNullException(String.Format("Object {0} is null, please input a value.", objectName));
            }

            if (objectToCheck is string)
            {
                if (string.IsNullOrWhiteSpace(objectToCheck.ToString()))
                {
                    throw new ArgumentNullException(nameof(objectName), string.Format("string {0} is null or is just whitespace", objectName));
                }
            }
        }
    }
}