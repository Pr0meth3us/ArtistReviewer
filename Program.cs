﻿namespace ArtistReviews
{
    using System.Configuration;
    using System;
    using System.Data.SqlClient;
    using System.Text.RegularExpressions;
    using System.Threading;
    using ArtistReviews.Properties;
    using System.Windows.Forms;

    class Program
    {

        /// <summary>
        /// Entry point.
        /// </summary>
        /// <param name="args">optional arguments.</param>
        static void Main(string[] args)
        {
            try
            {
                new MainMenu().mainMenu();
            }
            catch (SqlException sE) when (sE.Message == Resources.IncorrectSQLName ||
                                          sE.Message.Contains(Resources.LoginFailed))
            {
                var sqlServiceFunctions = new SQLServiceFunctions();

                var correctlogin = false;
                var count = 0;
                Console.WriteLine("Input correct server name, database name, user Name and Password");
                while (correctlogin == false)
                {
                    sqlServiceFunctions.changeSQLCOnnectionString();
                    SqlConnection Connection =
                        new SqlConnection(ConfigurationManager.AppSettings["SQLConnectionString"]);
                    try
                    {
                        Connection.Open();
                        Connection.Close();
                        correctlogin = true;
                    }
                    catch (SqlException sqE)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        count += 1;
                        Console.WriteLine("\n" + sqE.Message + "\n");
                        Console.ResetColor();
                        Console.WriteLine("{0} tries left", 3 - count);
                    }

                    if (count >= 3)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(Resources.LoginUnsuccessful);
                        Console.ReadLine();
                        Environment.Exit(0);
                    }
                }

                Console.WriteLine("Login Successful!");
                new MainMenu().mainMenu();

            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.ReadLine();
            }
        }

    }
}

