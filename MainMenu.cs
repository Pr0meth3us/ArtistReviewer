﻿namespace ArtistReviews
{
    using System;
    using System.Configuration;
    using System.Data.SqlClient;

    public class MainMenu
    {
        /// <summary>
        /// New instance of SQLQueries
        /// </summary>
        private SQLQueries querie = new SQLQueries();

        /// <summary>
        /// Main menu of program.
        /// </summary>
        public void mainMenu()
        {
            try
            {
                var querie = new SQLQueries();
                var guards = new Guards();
                querie.Connection.Open();
                var end = false;
                while (end != true)
                {
                    Console.WriteLine(
                        "Select options :" +
                        "\n 1) add new review, if the artist doesn't already exist it will be created," +
                        "\n 2) for all artists " +
                        "\n 3) for all reviews for a given artist " +
                        "\n 4) for an average review score of an artist " +
                        "\n 5) A random artist " +
                        "\n 6) To Delete a review for an artist " +
                        "\n 7) To exit the menu ");

                    try
                    {
                        var input = Convert.ToInt32(Console.ReadLine());
                        switch (input)
                        {
                            case 1:
                            {

                                Console.WriteLine("\n Input Reviewer name:");
                                var Reviewer = Console.ReadLine();
                                Console.WriteLine("\n Input artist:");
                                var Artist = Console.ReadLine();
                                Console.WriteLine("\n Input score:");
                                var Score = Convert.ToInt32(Console.ReadLine());
                                guards.Guard(Reviewer, nameof(Reviewer));
                                guards.Guard(Artist, nameof(Artist));
                                querie.testCreateReview(Artist.ToLower(), Reviewer, Score);
                                break;
                            }

                            case 2:
                            {
                                querie.getAllBands();
                                break;
                            }

                            case 3:
                            {
                                Console.WriteLine("Input Artist that this reviewer reviewed");
                                var Artist = Console.ReadLine();
                                guards.Guard(Artist, nameof(Artist));
                                querie.getAllReviewsOfBand(Artist.ToLower());
                                break;

                            }

                            case 4:
                            {
                                Console.WriteLine("Input Artist that this reviewer reviewed");
                                var Artist = Console.ReadLine();
                                querie.getAllReviewsOfBand(Artist.ToLower(), true);
                                break;
                            }
                            case 5:
                            {
                                querie.getRandomEmptyBand();
                                break;

                            }
                            case 6:
                            {
                                Console.WriteLine("Input Artist that this reviewer reviewed");
                                var Artist = Console.ReadLine();
                                Console.WriteLine("Input reviewer:");
                                var reviewer = Console.ReadLine();
                                guards.Guard(reviewer, nameof(reviewer));
                                guards.Guard(Artist, nameof(Artist));
                                querie.deleteReviewer(Artist.ToLower(), reviewer);
                                break;
                            }

                            case 7:
                            {
                                end = true;
                                break;
                                }

                            default:
                            {
                                Console.WriteLine("Enter a valid choice between 1 and 7.");
                                break;

                            }
                        }
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    catch (ArgumentNullException AN)
                    {
                        Console.WriteLine(AN.Message);
                    }
                }
                querie.Connection.Close();
            }
            catch
            {
                querie.Connection.Close();
                throw;
            }
        }
    }
}