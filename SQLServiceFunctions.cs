﻿namespace ArtistReviews
{
    using System;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Dynamic;
    using System.Data.SqlServerCe;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Deals with functions that set up the SQL connection.
    /// </summary>
    public class SQLServiceFunctions
    {
        /// <summary>
        /// Changes the app.config connection key.
        /// </summary>
        public void changeSQLCOnnectionString()
        {


            Console.WriteLine(@"Input server name (for example TOBY\LOCALSERVER): ");
            var serverName = Console.ReadLine();
            Console.WriteLine(@"Input DatabaseName: ");
            var databaseName = Console.ReadLine();
            Console.WriteLine("Input User name: ");
            var userName = Console.ReadLine();
            Console.WriteLine("Input Password");
            var Password = Console.ReadLine();

            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var oldSQL = ConfigurationManager.AppSettings["SQLConnectionString"];

            var serverRegex = new Regex(@"(?<=Server=)(.*)(?=;I)");
            var dataBaseRegex = new Regex(@"(?<=Initial Catalog=)(.*)(?=;U)");
            var userRegex = new Regex(@"(?<=User ID=)(.*)(?=;P)");
            var passwordRegex = new Regex(@"(?<=Password=)(.*)(?=;T)");

            var updateSQL = serverRegex.Replace(oldSQL, serverName);
            var update1SQL = dataBaseRegex.Replace(updateSQL, databaseName);
            var update2SQL = userRegex.Replace(update1SQL, userName);
            var update3SQL = passwordRegex.Replace(update2SQL, Password);

            config.AppSettings.Settings["SQLConnectionString"].Value = update3SQL;
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");

        }
    }
}